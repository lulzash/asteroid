<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Client;
// use App\Http\Requests\AestroidRequest;

class AsteroidController extends Controller
{
    public function index(Request $request)
    {
        $listData = [];
        $stats = [];
        $postData = [
            'start_date' => '',
            'end_date' => ''
        ];
        $chartData = [];
        if($request->isMethod('post')){
            $postData = $request->only('start_date', 'end_date');
            $validator = $this->getValidateAsteroid($postData);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $client = new Client();
            $url = 'https://api.nasa.gov/neo/rest/v1/feed?';
            $url .= 'start_date='.$request->start_date;
            $url .= '&end_date='.$request->end_date;
            $url .= "&api_key=8whuxojj1n82s6qhpai3HEfLqFiX37JFetkYBtGo";
            $res = $client->request('GET', $url);

            if ($res->getStatusCode() == 200) { // 200 OK
                $response_data = $res->getBody()->getContents();
                $listData = json_decode($response_data, true);
                // dd($listData);
                $asteroidSpeedArr = [];
                $asteroidCloseArr = [];                
                foreach($listData['near_earth_objects'] as $date => $neo){
                    $chartData['labels'][] = date('d-m-Y', strtotime($date));
                    $chartData['count'][] = count($neo);
                    foreach ($neo as $key => $data) {
                        $speed = current($data['close_approach_data'])['relative_velocity']['kilometers_per_second'];
                        $close = current($data['close_approach_data'])['miss_distance']['kilometers'];
                        $id = $data['id'];
                        $asteroidSpeedArr[$id] = $speed;
                        $asteroidCloseArr[$id] = $close;
                    }
                }
                $stats['fastest'] = [ array_search (max($asteroidSpeedArr), $asteroidSpeedArr) => max($asteroidSpeedArr)];
                $stats['close'] = [ array_search (min($asteroidSpeedArr), $asteroidSpeedArr) => min($asteroidSpeedArr)];
            }
        }
        return view('welcome', compact('listData', 'stats', 'postData', 'chartData'));
    }

    public function getValidateAsteroid($data){
        // $date1 = date_create($data['end_date']);
        // $date2 = date_create($data['start_date']);
        // $diff = date_diff($date1,$date2);
        // $diff->format("%a")
        $rules = [
            'start_date' => ['required','before:end_date'],
            'end_date' => ['required']
        ];
        $errMsg = [
            'start_date' => 'Please select start date.',
            'end_date' => 'Please select end date.'
        ];
        return Validator::make($data, $rules, $errMsg);
    }
}
