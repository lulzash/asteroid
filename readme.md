# Getting started

## Installation

Please check the official laravel installation guide for server requirements before you start. [Official Documentation](https://laravel.com/docs/5.4/installation#installation)


Clone the repository

    git clone https://lulzash@bitbucket.org/lulzash/asteroid.git

Switch to the repo folder

    cd asteroid

Install all the dependencies using composer

    composer install

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Generate a new application key

    php artisan key:generate

Start the local development server

You can now access the server at http://localhost/asteroids
    
**Make sure you set the correct database connection information before running the migrations** [Environment variables](#environment-variables)


## API Specification

> [Full API Spec](https://api.nasa.gov)



## Folders

- `app` - Contains all the Eloquent models
- `app/Http/Controllers` - Contains all the controllers
- `app/Http/Middleware` - Contains the JWT auth middleware
- `database/factories` - Contains the model factory for all the models
- `database/migrations` - Contains all the database migrations
- `routes` - Contains all the routes defined in web.php file


## Creators

*Akshay Dhalwar*


* <https://twitter.com/lulz45h>

* <https://github.com/lulzash>

* <https://stackoverflow.com/users/10808030/lulzash>

