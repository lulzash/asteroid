@extends('layouts.app')
@section('content')
<div class="container">
    <div class="jumbotron">
        <h1 class="display-5 text-center">Asteroid - Neo Stats</h1>
    </div>
    <!-- Content here -->
    <div class="row">
        <div class="col-12">
            <form action="{{ route('list') }}" method="POST"> 
            @csrf
                <div class="row">
                    <div class="col">
                        <input type="date" name="start_date" class="form-control" value="{{ old('start_date', $postData['start_date']) }}" placeholder="Start Date">
                        @if ($errors->has('start_date'))
                            <span class="text-danger" role="alert">
                                <small>{{ $errors->first('start_date') }}</small>
                            </span>
                        @endif
                    </div>
                    <div class="col">
                        <input type="date" name="end_date" class="form-control" value="{{ old('end_date', $postData['end_date']) }}" placeholder="End Date">
                        @if ($errors->has('end_date'))
                            <span class="text-danger" role="alert">
                                <small>{{ $errors->first('end_date') }}</small>
                            </span>
                        @endif
                    </div>
                    <div class="col">
                        <button type="submit" class="btn btn-primary btn-block">Search</button>
                    </div>
                </div>
            </form>
        </div>                
    </div>
    @if(!empty($stats))
    <div class="row mt-4">
        <div class="col">
            <ul class="list-unstyled">
                <li><strong>Fastest Asteroid</strong>
                    <ul>
                        <li>Asteroid ID : {{ key($stats['fastest']) }}</li>
                        <li>Speed : {{ reset($stats['fastest']) }} km/h</li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="col">
            <ul class="list-unstyled">
                <li><strong>Closest Asteroid</strong>
                    <ul>
                        <li>Asteroid ID :  {{ key($stats['close']) }}</li>
                        <li>Distance : {{ reset($stats['close']) }}</li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="col">
            <ul class="list-unstyled">
                <li><strong>Average Size of the Asteroids</strong>
                    <ul>
                        <li>Size : </li>
                    </ul>
                </li>
            </ul>
        </div>            
    </div>
    <div class="row mt-4">
        <div class="col">
            <canvas id="myChart"></canvas>
        </div>
    </div>
    @endif  
</div>
@endsection

@section('custom_script')     
    @if(!empty($chartData))
    <script>
        var ctx = document.getElementById('myChart');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: @json($chartData['labels']), // dates here
                datasets: [{
                    label: 'No. of Asteroid',
                    data: @json($chartData['count']), // count here
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    </script>
    @endif
@endsection
    
